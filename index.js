// console.log("hello world")
// [SECTION] - Parameters and Arguments
// FUnctions in JS are lines/blocks of codes that tell our devilce/application to perform a certain task.

// Last session we learned how to use a basic function

function printInput(){
    let nickname = prompt("Enter your nickname.");
    console.log("Hi, " + nickname); 
}

printInput();

// In some casess a basic function may not be ideal
// For other cases, functions can also process data directly into it instead.
// Consider this function
// parameter is located inside the "(parameter)" after the function name.
function printName(name){
    console.log("My name is " + name);
} 

// Argument is located in the invocation of the function. The data then will be passed on the parameter which can be used inside the code blocks of a particular function.
printName("Juana");
printName("John");
printName("Jane");

// variables cam also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);

// Function arguments cannot be used by a function if there are no parameters provided.

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + "divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(50);

// Functions As Arguments

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed");
}
function invokeFunction(argumentFunction){
    argumentFunction();
}

invokeFunction(argumentFunction);
// finding more info about a function, we can use conolse.log([function_name]);
console.log(argumentFunction);

// Function - Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters"

function createFullName(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");

// "Juan" will be stored in the parameter "firstName"
// "Juan" will be stored in the parameter "middleName"
// "Juan" will be stored in the parameter "lastName"

// The "lastname" parameterwill return undefined value
createFullName("Juan", "Dela");
// "Hello" will be disregarded since the function is expecting 3 arguments only.
createFullName("Juan", "Dela", "Cruz", "Hello");

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the argument.
// the order of the argument is the same to the order of the parameters.

function printFullName(middleName, firstName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("Juan", "Dela", "Cruz");

// The Return Statement 

function returnFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
    console.log("This message will not be printed ");
}
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

// returnFullName("Jeffrey", "Smith", "Bezos"); --> This will not work if our function is expectinng a return value, returned values should be stored in a variable.

console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country){
    let fullAddress = city + ", " + country;
    return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// On the other hand, when a function only has console.log() to display its result will return undefined instead.

function printPlayerInfo(username, level, job){
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);

printPlayerInfo("knight_white", 95, "Paladin");

// returns undefined because printPlayerInfo returns nothing.